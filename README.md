# gitlab-ci-tools

Some handy CLI tools for working with GitLab CI if you're not a fan of switching back and forth between your editor and a web browser. For a bit of background, see my [blog post](https://ajc.me/posts/gitlab-ci-tools/).

So far:

- `gitlab-ci-status` : check the status of a Gitlab CI pipeline.
- `gitlab-ci-wait`   : use `gitlab-ci-status` to wait for completion of a Gitlab CI pipeline.

# Setup

You need to provide:

- your Gitlab host via the `-g` option or via `GITLAB_HOST` in your environment.

You can provide:

- a Gitlab token with "read_api" permissions, via the `-t` option or via `GITLAB_TOKEN` in your environment. otherwise we fallback to trying [`gopass`](https://github.com/gopasspw/gopass) and looking for `ci-status` under a pathname that matches whatever you gave as your Gitlab hostname (eg. `my.gitlab.org/ci-status`).

# Dependencies

- `bash`, `curl`, `grep`, `awk`, `sed`.
- `git`, if you want it to guess the name of your Gitlab org/repo.
- GNU `date` (used in `gitlab-ci-wait`'s `-v` output - something for me to fix, later).
- `notify-send` if you want desktop notifications when your pipeline finishes (see `gitlab-ci-wait -n`).
- [jq](https://stedolan.github.io/jq/).

# Examples

``` shell
% gitlab-ci-status
running

[ ... time passes ... ]

% gitlab-ci-status
success
```

``` shell
% gitlab-ci-wait -v
success for some-org/local-dns in 26 seconds at Sun 12 Jul 2020 10:14:22 AEST 
```

# Credits, Licensing

Andrew J Cosgriff wrote this in 2020-07, and released it under the [MIT License](./LICENSE).