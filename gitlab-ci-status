#! /bin/bash
#
# gitlab-ci-status : check the status of a Gitlab CI pipeline
#
script=$0

help () {
  echo "Syntax : $(basename $0) [-a|-i|-n <n>|-p <id>|-g <host>|-t <token>] [org/repo]"
  echo "    -a : show the whole JSON response."
  echo "    -i : just show the pipeline ID."
  echo "    -n <n> : show results for the nth build back from the current one."
  echo "    -p <id> : show results for the given pipeline ID."
  echo
  echo "    -g <host> : hostname for your Gitlab instance. We also check \$GITLAB_HOST. Default: ${gitlab}."
  echo "    -t <token> : Gitlab token. Default: \$GITLAB_TOKEN or gopass from the <gitlab-host>/ci-status path."
  echo
  echo "If no org/repo is supplied, we will look at where 'origin' points to in the local git repository."
  echo
  echo "Exit codes:"
  awk '/case "\$pipeline_status"/,/esac/' $script | grep ';;' | sed -e 's/ ret=/ /g' | sed -e 's/;;//g'
  exit 9
}

gitlab=${GITLAB_HOST:-gitlab.example.com}
debug=0
all=0
index=0
pipeline_id=
fetch_pipeline_id=0
token=${GITLAB_TOKEN}
while getopts ahdin:p:t:g: c
do
    case $c in
        a) all=1 ;;
        h) help ;;
        d) debug=1 ;;
        i) fetch_pipeline_id=1 ;;
        n) index=$OPTARG ;;
        p) pipeline_id=$OPTARG ;;
        t) token=$OPTARG ;;
        g) gitlab=$OPTARG ;;
    esac
done
shift $((OPTIND - 1))

if [[ "$gitlab" == "gitlab.example.com" ]]; then
    echo "I need a GitLab host to talk to."
    echo "Could you try setting \$GITLAB_HOST, or using the -g option?"
    echo
    echo "\"$(basename $0) -h\" gives more help on options."
    echo
    exit 9
fi

# guess the org/repo if we weren't given one
repo=$1
if [[ "${repo}" == "" ]]; then
    origin=$(git remote show -n origin | grep '^[ ]*Push' | sed -e 's/^[ ]*Push[ ]*URL: //g')
    if echo "${origin}" | grep -q "${gitlab}"; then
        repo=$(echo "${origin}" | sed -e "s;^.*${gitlab}[:/]\(.*\)\.git;\1;g")
    fi
fi
url_encoded_repo=$(echo $repo | sed -e 's@/@%2f@g')

[[ "${token}" == "" ]] && token=$(gopass show ${gitlab}/ci-status)

if [[ -z "$pipeline_id" ]]; then
    results=$(curl -s --header "Private-Token: ${token}" \
                   "https://${gitlab}/api/v4/projects/${url_encoded_repo}/pipelines" \
                  | jq ".[${index}]")
else
    results=$(curl -s --header "Private-Token: ${token}" \
                   "https://${gitlab}/api/v4/projects/${url_encoded_repo}/pipelines/${pipeline_id}" \
                  | jq .)
fi

pipeline_status=$(echo $results | jq -r .status)

case "$pipeline_status" in
    success)  ret=0 ;;
    failed)   ret=1 ;;
    canceled) ret=2 ;;
    skipped)  ret=3 ;;
    pending)  ret=4 ;;
    running)  ret=5 ;;
    created)  ret=6 ;;
    manual)   ret=7 ;;
    null)     ret=8 ;;
    *)        ret=9 ;;
esac

if [[ "$all" == "1" ]]; then
    echo ${results} | jq .
elif [[ "$fetch_pipeline_id" == "1" ]]; then
    ret=0
    echo ${results} | jq -r ".id"
else
    echo ${pipeline_status}
fi
exit $ret
